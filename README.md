## My Portfolio
Welcome to my portfolio! This website was built mainly with React, and showcases my skills and projects as a web developer. You can visit the live website here.

https://gomezbrandon.info/

## Features
```
- Responsive design
- Smooth animations using React Tilt
- Project showcase with images, descriptions, and links
- Skills section with icons and descriptions
- Contact form with validation and email sending functionality
```


## Technologies
This project was built with the following technologies:

```
- React
- EmailJS
- Tailwind CSS
- Three JS
- react-particles
- tsparticles
```

## Languages

```
- CSS
- HTML
- Javascript
```



## Installation
To run this project locally, follow these steps:

```
[ ] Clone the repository
[ ] cd to the project directory
[ ] Install the dependencies: npm install
[ ] Start the development server: npm run dev
```

## Usage
Feel free to use this project as a template for your own portfolio.
You can customize the content, styling, and functionality to fit your needs. Here are some tips to get you started:

```
- Replace the images and descriptions in the src/Constants/constants.js file with your own projects.
- Update the skills icons and descriptions in the src/Constants/constants.js file with your own skills.
- Customize the styling in the src/App/index.css directory to match your personal brand.
- Modify the src/Components/ContactForm.js file to use your own email sending service or API.
```
