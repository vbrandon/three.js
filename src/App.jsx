import { BrowserRouter } from "react-router-dom";
import { About, Contact, Experience, Hero,
  Navbar, Tech, Works } from './components';
import './assets/static/stars.css';
import { stars } from "./assets";


const App = () => {

  return (
    <div>
      <BrowserRouter>
      <div className="relative z-0 bg-primary">
        <div className="bg-hero-pattern bg-cover bg-no-repeat bg-center">
          <Navbar />
          <Hero />
          </div>
          <About />
          <Experience />
          <Tech />
          <Works />
          <div className="relative z-1">
            <img src={stars} alt='stars' className='stars'/>
          <Contact />
          </div>
      </div>
      </BrowserRouter>
    </div>
  )
}

export default App
