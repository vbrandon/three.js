import logo from "./logo.svg";
import backend from "./backend.png";
import mobile from "./mobile.png";
import github from "./github.png";
import menu from "./menu.svg";
import close from "./close.svg";
import green from "./green.png";
import stars from "./stars.png";
import herobg from "./herobg.png";


import css from "./tech/css.png";
import docker from "./tech/docker.png";
import git from "./tech/git.png";
import html from "./tech/html.png";
import javascript from "./tech/javascript.png";
import mongodb from "./tech/mongodb.png";
import nodejs from "./tech/nodejs.png";
import reactjs from "./tech/reactjs.png";
import tailwind from "./tech/tailwind.png";
import postgresSQL from "./tech/postgresSQL.svg";



import hricon from "./company/hricon.png";
import thdlogo from "./company/thdlogo.png";
import python from "./tech/python.png";



export {
  herobg,
  stars,
  green,
  python,
  postgresSQL,
  thdlogo,
  hricon,
  logo,
  backend,
  mobile,
  github,
  menu,
  close,
  css,
  docker,
  git,
  html,
  javascript,
  mongodb,
  nodejs,
  reactjs,
  tailwind,
};
