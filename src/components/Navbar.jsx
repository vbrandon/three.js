import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { styles } from '../styles';
import { navLinks } from '../constants';
import { logo, menu, close } from '../assets';

const Navbar = () => {
  const [active, setActive] = useState('');
  const [toggle, setToggle] = useState(false);

  const handleScrollToSection = (id) => {
    const section = document.getElementById(id);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <nav className={`${styles.paddingX} w-full flex items-center py-5 fixed top-0 z-20 bg-primary`}>
      <div className="flex w-full justify-between items-center max-w-7xl mx-auto">
        <Link
          to="/"
          className="flex items-center gap-2"
          onClick={() => {
            setActive('');
            window.scrollTo(0, 0);
          }}
        >
          <img src={logo} alt="logo" className="object-contain h-9 w-9 " />
          <p className="text-white text-[18px] font-bold cursor-pointer flex">
            Brandon Gomez &nbsp;
            <span className="sm:block hidden">| Software Engineer </span>
          </p>
        </Link>
        <ul className="list-none hidden sm:flex flex-row gap-10">
          {navLinks.map((link) => (
            <li
              key={link.id}
              className={`${
                active === link.title ? 'text-white' : 'text-secondary'
              } hover:text-white text-[18px] font-medium cursor-pointer`}
              onClick={() => {
                setActive(link.title);
                handleScrollToSection(link.id);
              }}
            >
              <span>{link.title}</span>
            </li>
          ))}
        </ul>
        <div className="sm:hidden">
          <button
            onClick={() => setToggle(!toggle)}
            className="outline-none focus:outline-none"
          >
            {toggle ? (
              <img src={close} className="h-6 w-6" alt="close" />
            ) : (
              <img src={menu} className="h-6 w-6" alt="menu" />
            )}
          </button>
        </div>
      </div>
      {toggle && (
        <div className="sm:hidden w-full h-screen bg-primary z-20 absolute top-16 left-0">
          <ul className="list-none w-full h-full flex flex-col justify-center items-center">
            {navLinks.map((link) => (
              <li
                key={link.id}
                className={`${
                  active === link.title ? 'text-white' : 'text-secondary'
                } hover:text-white text-[18px] font-medium cursor-pointer`}
                onClick={() => {
                  setActive(link.title);
                  handleScrollToSection(link.id);
                  setToggle(false);
                }}
              >
                <span>{link.title}</span>
              </li>
            ))}
          </ul>
        </div>
      )}
    </nav>
  );
};

export default Navbar;
