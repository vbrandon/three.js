import ParticlesComponent from "./ParticlesComponent";
import { heroOptions } from "./particlesOption";

const HeroParticles = () => {
return (
    <div className="w-full h-screen  min-h-[800px]">
    <ParticlesComponent
        id="hero-particles"
        className="w-full hover:brightness-150 grayscale h-screen z-0 "
        particlesOptions={heroOptions}
    />
    </div>
);
};

export default HeroParticles;
