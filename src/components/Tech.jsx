import React from "react";
import { SectionWrapper } from "../hoc";
import { technologies } from "../constants";

import { styles } from "../styles";
import Lottie from "lottie-react";
import TechStack from '../assets/TechStack.json';
import '../assets/static/icons.css';

const Tech = () => {
  return (
    <div className='flex flex-col font-primary'>
      <span className={`${styles.sectionHeadText} text-center`}>Tech Stack <Lottie animationData={TechStack} className="h-[380px] " />  </span>
      <div className='flex flex-row flex-wrap justify-center gap-8'>
        {technologies.map((technology) => {
          return (
            <div key={technology.name} className="icon-container w-1/6 flex flex-col items-center">
              <img className=' flex glow flex-row place-items-end items-center hover:shadow-xl-[#0d8799] hover:animate-pulse h-[70px]' src={technology.icon} alt={technology.name} />
              <span className='font-semibold text-[15px] flex justify-center items-center'>{technology.name} </span>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SectionWrapper(Tech, '');
