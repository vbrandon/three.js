import Lottie from "lottie-react";
import working from './../../assets/working.json';




export default function Computers() {

  return (
    <div className='flex flex-row relative'> <Lottie animationData={working} className="w-4/6" />  </div>
  )
}
