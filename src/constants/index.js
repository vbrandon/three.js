
import {
    herobg,
    green,
    thdlogo,
    mobile,
    backend,
    javascript,
    html,
    css,
    reactjs,
    tailwind,
    nodejs,
    postgresSQL,
    mongodb,
    git,
    python,
    docker,
    hricon,
  } from "../assets";





  export const navLinks = [
    {
      id: "about",
      title: "About",
    },
    {
      id: "work",
      title: "Work",
    },
    {
      id: "contact",
      title: "Contact",
    },
  ];

  const services = [

    {
      title: "Frontend Developer",
      icon: mobile,
    },
    {
      title: "Backend Developer",
      icon: backend,
    },
  ];

  const technologies = [
    {
      name: "HTML 5",
      icon: html,
    },
    {
      name: "CSS 3",
      icon: css,
    },
    {
      name: "JavaScript",
      icon: javascript,
    },
    {
      name: "Python",
      icon: python,
    },
    {
      name: "React",
      icon: reactjs,
    },
    {
      name: "PostgreSQL",
      icon: postgresSQL,
    },

    {
      name: "Tailwind CSS",
      icon: tailwind,
    },
    {
      name: "Node JS",
      icon: nodejs,
    },
    {
      name: "MongoDB",
      icon: mongodb,
    },
    {
      name: "Git",
      icon: git,
    },
    {
      name: "Docker",
      icon: docker,
    },
  ];

  const experiences = [
    {
      title: "Head Cashier",
      company_name: "Home Depot",
      icon: thdlogo,
      iconBg: "#383E56",
      date: "Jul 2019 - Feb 2022",
      points: [
        "Managed and organized cash register operations, demonstrating attention to detail and problem-solving skills, resulting in a decrease in customer wait times by 25%.",
        "Collaborated effectively with customers and team to maintain a positive and productive work environment, achieving team goals and objectives through effective communication and task delegation.",

      ],
    },
    {
      title: "Student FullStack Developer",
      company_name: "Hack Reactor",
      icon: hricon,
      iconBg: "#E6DEDD",
      date: "Dec 2022 - May 2023",
      points: [
        "19-week software engineering immersive program with 1000+ hours of coding. Full-Stack development with JavaScript, Python, Django, React, Redux, PostgreSQL, MongoDB, and MySQL in an AGILE environment.",
        "Relevant Coursework: Node, Express, jQuery, HTML, CSS, Caprover, Docker, AWS, JSON, Nomad, and Consul",
      ],
    },
  ];


  const projects = [
    {
      name: "Green de la Creme",
      description:
        "Web based application used to gain useful tips and tricks for gardening and/or to share plants with others.",
      tags: [
        {
          name: "React",
          color: "blue-text-gradient",
        },
        {
          name: "PostgresSQL",
          color: "green-text-gradient",
        },
        {
          name: "Tailwind",
          color: "pink-text-gradient",
        },
        {
          name: "Python",
          color: "blue-text-gradient",
        },
        {
          name: "Javascript",
          color: "green-text-gradient",
        },
        {
          name: "RestAPI",
          color: "pink-text-gradient",
        },
        {
          name: "Git",
          color: "pink-text-gradient",
        },
        {
          name: "Docker",
          color: "pink-text-gradient",
        },
      ],
      image: green,
      source_code_link: "https://gitlab.com/green-de-la-creme/green-de-la-creme",
    },
    {
      name: "Conference-Go",
      description:
        "Conference scheduling app which implements FastAPI to enhance user experience.",
      tags: [
        {
          name: "Python",
          color: "blue-text-gradient",
        },
        {
          name: "RestAPI",
          color: "green-text-gradient",
        },
        {
          name: "CSS",
          color: "pink-text-gradient",
        },
        {
          name: "Git",
          color: "pink-text-gradient",
        },
        {
          name: "Docker",
          color: "pink-text-gradient",
        },
        {
          name: "Tailwind",
          color: "pink-text-gradient",
        },
      ],
      image: herobg,
      source_code_link: "https://github.com/",
    },
  ];

  export { services, technologies, experiences, projects };
